<?php

namespace AppBundle\Repository;

/**
 * InmuebleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InmuebleRepository extends \Doctrine\ORM\EntityRepository
{
	public function findActive(){
		$em = $this->getEntityManager(); 
		$qb = $em->createQueryBuilder();
		$qb->select('i')
		->from('AppBundle:Inmueble', 'i')
		->where('i.status = 1 OR i.status = 3');
		$query = $qb->getQuery();
		return $query->getResult();
	}

	public function buscar($data){
	$em = $this->getEntityManager(); 
	$qb = $em->createQueryBuilder();
	$qb->select('i')
	->from('AppBundle:Inmueble', 'i')
	->where('i.precio >= :min AND i.precio <= :max')
	->setParameters(['min'=>$data['min'],'max'=>$data['max']]);
	if ($data['tipo']) {
		$qb->andWhere('i.tipo_inmueble = :tipo')
		->setParameter('tipo',$data['tipo']);
	}
	$query = $qb->getQuery();
	return $query->getResult();
	}
}
