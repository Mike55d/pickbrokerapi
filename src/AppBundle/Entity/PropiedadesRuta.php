<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PropiedadesRuta
 *
 * @ORM\Table(name="propiedades_ruta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PropiedadesRutaRepository")
 */
class PropiedadesRuta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="latLong", type="string", length=255)
     */
    private $latLong;

    /**
     * @var string
     *
     * @ORM\Column(name="comentarios", type="string", length=255)
     */
    private $comentarios;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha", type="datetime", length=255)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255)
     */
    private $imagen;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set direccion.
     *
     * @param string $direccion
     *
     * @return PropiedadesRuta
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion.
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set latLong.
     *
     * @param string $latLong
     *
     * @return PropiedadesRuta
     */
    public function setLatLong($latLong)
    {
        $this->latLong = $latLong;

        return $this;
    }

    /**
     * Get latLong.
     *
     * @return string
     */
    public function getLatLong()
    {
        return $this->latLong;
    }

    /**
     * Set comentarios.
     *
     * @param string $comentarios
     *
     * @return PropiedadesRuta
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get comentarios.
     *
     * @return string
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Set imagen.
     *
     * @param string $imagen
     *
     * @return PropiedadesRuta
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen.
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return PropiedadesRuta
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return PropiedadesRuta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }
}
