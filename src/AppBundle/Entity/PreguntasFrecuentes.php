<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PreguntasFrecuentes
 *
 * @ORM\Table(name="preguntas_frecuentes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PreguntasFrecuentesRepository")
 */
class PreguntasFrecuentes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta", type="string", length=255)
     */
    private $pregunta;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta", type="string", length=255)
     */
    private $respuesta;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pregunta.
     *
     * @param string $pregunta
     *
     * @return PreguntasFrecuentes
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta.
     *
     * @return string
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set respuesta.
     *
     * @param string $respuesta
     *
     * @return PreguntasFrecuentes
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    /**
     * Get respuesta.
     *
     * @return string
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }
}
