<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cita
 *
 * @ORM\Table(name="cita")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CitaRepository")
 */
class Cita
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    */

    private $user;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="socio", referencedColumnName="id")
    */

    private $socio;


    /**
    * @ORM\ManyToOne(targetEntity="Inmueble")
    * @ORM\JoinColumn(name="inmueble", referencedColumnName="id")
    */

    private $inmueble;

    /**
    * @ORM\ManyToOne(targetEntity="StatusCita")
    * @ORM\JoinColumn(name="status", referencedColumnName="id")
    */
    private $status;

    /**
     * @var \string
     *
     * @ORM\Column(name="comentario", type="string")
     */
    private $comentario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora", type="time")
     */
    private $hora;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha.
     *
     * @param \DateTime $fecha
     *
     * @return Cita
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha.
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set hora.
     *
     * @param \DateTime $hora
     *
     * @return Cita
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora.
     *
     * @return \DateTime
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return Cita
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set socio.
     *
     * @param \AppBundle\Entity\User|null $socio
     *
     * @return Cita
     */
    public function setSocio(\AppBundle\Entity\User $socio = null)
    {
        $this->socio = $socio;

        return $this;
    }

    /**
     * Get socio.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getSocio()
    {
        return $this->socio;
    }

    /**
     * Set comentario.
     *
     * @param string $comentario
     *
     * @return Cita
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario.
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set inmueble.
     *
     * @param \AppBundle\Entity\Inmueble|null $inmueble
     *
     * @return Cita
     */
    public function setInmueble(\AppBundle\Entity\Inmueble $inmueble = null)
    {
        $this->inmueble = $inmueble;

        return $this;
    }

    /**
     * Get inmueble.
     *
     * @return \AppBundle\Entity\Inmueble|null
     */
    public function getInmueble()
    {
        return $this->inmueble;
    }

    /**
     * Set status.
     *
     * @param \AppBundle\Entity\StatusCita|null $status
     *
     * @return Cita
     */
    public function setStatus(\AppBundle\Entity\StatusCita $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \AppBundle\Entity\StatusCita|null
     */
    public function getStatus()
    {
        return $this->status;
    }
}
