<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inmueble
 *
 * @ORM\Table(name="inmueble")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InmuebleRepository")
 */
class Inmueble
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user", referencedColumnName="id")
    */
    private $user;

    /**
    * @ORM\ManyToOne(targetEntity="StatusPublicacion")
    * @ORM\JoinColumn(name="status", referencedColumnName="id")
    */

    private $status; 

    /**
    * @ORM\ManyToOne(targetEntity="TipoPublicacion")
    * @ORM\JoinColumn(name="tipo_publicacion", referencedColumnName="id")
    */
    private $tipo_publicacion; 

    /**
    * @ORM\ManyToOne(targetEntity="TipoInmueble")
    * @ORM\JoinColumn(name="tipo_inmueble", referencedColumnName="id")
    */
    private $tipo_inmueble; 


    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="inmobiliaria", type="string", length=255)
     */
    private $inmobiliaria;

    /**
     * @var int
     *
     * @ORM\Column(name="precio", type="integer")
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="terreno", type="string", length=255)
     */
    private $terreno;

    /**
     * @var string
     *
     * @ORM\Column(name="construido", type="string", length=255)
     */
    private $construido;

    /**
     * @var string
     *
     * @ORM\Column(name="recamaras", type="string", length=255)
     */
    private $recamaras;

    /**
     * @var string
     *
     * @ORM\Column(name="banios", type="string", length=255)
     */
    private $banios;

    /**
     * @var string
     *
     * @ORM\Column(name="cochera", type="string", length=255)
     */
    private $cochera;

    /**
     * @var string
     *
     * @ORM\Column(name="antiguedad", type="string", length=255)
     */
    private $antiguedad;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fechaPublicacion", type="datetime", length=255)
     */
    private $fechaPublicacion;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fechaVencimiento", type="datetime", length=255)
     */
    private $fechaVencimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="lat_long", type="string", length=255)
     */
    private $latLong;

    /**
     * @var string
     *
     * @ORM\Column(name="caracteristicas", type="string", length=255)
     */
    private $caracteristicas;

    /**
     * @var string
     *
     * @ORM\Column(name="servicios", type="string", length=255)
     */
    private $servicios;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo.
     *
     * @param string $titulo
     *
     * @return Inmueble
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo.
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set precio.
     *
     * @param int $precio
     *
     * @return Inmueble
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio.
     *
     * @return int
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set direccion.
     *
     * @param string $direccion
     *
     * @return Inmueble
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion.
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Inmueble
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set terreno.
     *
     * @param string $terreno
     *
     * @return Inmueble
     */
    public function setTerreno($terreno)
    {
        $this->terreno = $terreno;

        return $this;
    }

    /**
     * Get terreno.
     *
     * @return string
     */
    public function getTerreno()
    {
        return $this->terreno;
    }

    /**
     * Set construido.
     *
     * @param string $construido
     *
     * @return Inmueble
     */
    public function setConstruido($construido)
    {
        $this->construido = $construido;

        return $this;
    }

    /**
     * Get construido.
     *
     * @return string
     */
    public function getConstruido()
    {
        return $this->construido;
    }

    /**
     * Set recamaras.
     *
     * @param string $recamaras
     *
     * @return Inmueble
     */
    public function setRecamaras($recamaras)
    {
        $this->recamaras = $recamaras;

        return $this;
    }

    /**
     * Get recamaras.
     *
     * @return string
     */
    public function getRecamaras()
    {
        return $this->recamaras;
    }

    /**
     * Set banios.
     *
     * @param string $banios
     *
     * @return Inmueble
     */
    public function setBanios($banios)
    {
        $this->banios = $banios;

        return $this;
    }

    /**
     * Get banios.
     *
     * @return string
     */
    public function getBanios()
    {
        return $this->banios;
    }

    /**
     * Set cochera.
     *
     * @param string $cochera
     *
     * @return Inmueble
     */
    public function setCochera($cochera)
    {
        $this->cochera = $cochera;

        return $this;
    }

    /**
     * Get cochera.
     *
     * @return string
     */
    public function getCochera()
    {
        return $this->cochera;
    }

    /**
     * Set antiguedad.
     *
     * @param string $antiguedad
     *
     * @return Inmueble
     */
    public function setAntiguedad($antiguedad)
    {
        $this->antiguedad = $antiguedad;

        return $this;
    }

    /**
     * Get antiguedad.
     *
     * @return string
     */
    public function getAntiguedad()
    {
        return $this->antiguedad;
    }

    /**
     * Set fechaPublicacion.
     *
     * @param string $fechaPublicacion
     *
     * @return Inmueble
     */
    public function setFechaPublicacion($fechaPublicacion)
    {
        $this->fechaPublicacion = $fechaPublicacion;

        return $this;
    }

    /**
     * Get fechaPublicacion.
     *
     * @return string
     */
    public function getFechaPublicacion()
    {
        return $this->fechaPublicacion;
    }

    /**
     * Set fechaVencimiento.
     *
     * @param string $fechaVencimiento
     *
     * @return Inmueble
     */
    public function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento.
     *
     * @return string
     */
    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return Inmueble
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status.
     *
     * @param \AppBundle\Entity\StatusPublicacion|null $status
     *
     * @return Inmueble
     */
    public function setStatus(\AppBundle\Entity\StatusPublicacion $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \AppBundle\Entity\StatusPublicacion|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set tipoPublicacion.
     *
     * @param \AppBundle\Entity\TipoPublicacion|null $tipoPublicacion
     *
     * @return Inmueble
     */
    public function setTipoPublicacion(\AppBundle\Entity\TipoPublicacion $tipoPublicacion = null)
    {
        $this->tipo_publicacion = $tipoPublicacion;

        return $this;
    }

    /**
     * Get tipoPublicacion.
     *
     * @return \AppBundle\Entity\TipoPublicacion|null
     */
    public function getTipoPublicacion()
    {
        return $this->tipo_publicacion;
    }

    /**
     * Set tipoInmueble.
     *
     * @param \AppBundle\Entity\TipoInmueble|null $tipoInmueble
     *
     * @return Inmueble
     */
    public function setTipoInmueble(\AppBundle\Entity\TipoInmueble $tipoInmueble = null)
    {
        $this->tipo_inmueble = $tipoInmueble;

        return $this;
    }

    /**
     * Get tipoInmueble.
     *
     * @return \AppBundle\Entity\TipoInmueble|null
     */
    public function getTipoInmueble()
    {
        return $this->tipo_inmueble;
    }

    /**
     * Set latLong.
     *
     * @param string $latLong
     *
     * @return Inmueble
     */
    public function setLatLong($latLong)
    {
        $this->latLong = $latLong;

        return $this;
    }

    /**
     * Get latLong.
     *
     * @return string
     */
    public function getLatLong()
    {
        return $this->latLong;
    }

    /**
     * Set inmobiliaria.
     *
     * @param string $inmobiliaria
     *
     * @return Inmueble
     */
    public function setInmobiliaria($inmobiliaria)
    {
        $this->inmobiliaria = $inmobiliaria;

        return $this;
    }

    /**
     * Get inmobiliaria.
     *
     * @return string
     */
    public function getInmobiliaria()
    {
        return $this->inmobiliaria;
    }

    /**
     * Set caracteristicas.
     *
     * @param string $caracteristicas
     *
     * @return Inmueble
     */
    public function setCaracteristicas($caracteristicas)
    {
        $this->caracteristicas = $caracteristicas;

        return $this;
    }

    /**
     * Get caracteristicas.
     *
     * @return string
     */
    public function getCaracteristicas()
    {
        return $this->caracteristicas;
    }

    /**
     * Set servicios.
     *
     * @param string $servicios
     *
     * @return Inmueble
     */
    public function setServicios($servicios)
    {
        $this->servicios = $servicios;

        return $this;
    }

    /**
     * Get servicios.
     *
     * @return string
     */
    public function getServicios()
    {
        return $this->servicios;
    }
}
