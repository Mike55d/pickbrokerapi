<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiciosInmuebles
 *
 * @ORM\Table(name="servicios_inmuebles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiciosInmueblesRepository")
 */
class ServiciosInmuebles
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
    * @ORM\ManyToOne(targetEntity="Inmueble")
    * @ORM\JoinColumn(name="inmueble", referencedColumnName="id")
    */

    private $inmueble;

    /**
    * @ORM\ManyToOne(targetEntity="TipoServicio")
    * @ORM\JoinColumn(name="servicio", referencedColumnName="id")
    */

    private $servicio;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set inmueble.
     *
     * @param \AppBundle\Entity\Inmueble|null $inmueble
     *
     * @return ServiciosInmuebles
     */
    public function setInmueble(\AppBundle\Entity\Inmueble $inmueble = null)
    {
        $this->inmueble = $inmueble;

        return $this;
    }

    /**
     * Get inmueble.
     *
     * @return \AppBundle\Entity\Inmueble|null
     */
    public function getInmueble()
    {
        return $this->inmueble;
    }

    /**
     * Set servicio.
     *
     * @param \AppBundle\Entity\TipoServicio|null $servicio
     *
     * @return ServiciosInmuebles
     */
    public function setServicio(\AppBundle\Entity\TipoServicio $servicio = null)
    {
        $this->servicio = $servicio;

        return $this;
    }

    /**
     * Get servicio.
     *
     * @return \AppBundle\Entity\TipoServicio|null
     */
    public function getServicio()
    {
        return $this->servicio;
    }
}
