<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mensaje
 *
 * @ORM\Table(name="mensaje")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MensajeRepository")
 */
class Mensaje
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="envia", referencedColumnName="id")
    */

    private $envia;

    /**
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="recibe", referencedColumnName="id")
    */

    private $recibe;

/**
     * @var \String
     *
     * @ORM\Column(name="mensaje", type="string")
     */
    private $mensaje;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Mensaje
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set mensaje.
     *
     * @param string $mensaje
     *
     * @return Mensaje
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje.
     *
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set envia.
     *
     * @param \AppBundle\Entity\User|null $envia
     *
     * @return Mensaje
     */
    public function setEnvia(\AppBundle\Entity\User $envia = null)
    {
        $this->envia = $envia;

        return $this;
    }

    /**
     * Get envia.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getEnvia()
    {
        return $this->envia;
    }

    /**
     * Set recibe.
     *
     * @param \AppBundle\Entity\User|null $recibe
     *
     * @return Mensaje
     */
    public function setRecibe(\AppBundle\Entity\User $recibe = null)
    {
        $this->recibe = $recibe;

        return $this;
    }

    /**
     * Get recibe.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getRecibe()
    {
        return $this->recibe;
    }
}
