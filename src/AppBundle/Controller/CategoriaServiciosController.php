<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

/**
* @Route("/api/categoriaServicios")
* @SWG\Tag(name="categoriaServicios")
*/
class CategoriaServiciosController extends AbstractFOSRestController
{
	/**
	* @Route("/", methods="GET")
	* @SWG\Response(
	*   response=200,
	*   description="categorias de servicios",
	* )
	*/
	public function indexAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$categorias = $em->getRepository('AppBundle:CategoriaServicios')->findAll(); 
		$view = $this->view($categorias,200);
		return $this->handleView($view);
	}
}
