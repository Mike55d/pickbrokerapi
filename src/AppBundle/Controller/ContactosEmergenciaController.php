<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\ContactoEmergencia;

/**
* @Route("/api/contactosEmergencia")
* @SWG\Tag(name="contactosEmergencia")
*/
class ContactosEmergenciaController extends AbstractFOSRestController
{
		/**
	* @Route("/new", methods="POST")
	* @SWG\Response(
	*   response=200,
	*   description="nuevo contacto de emergencia ",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="datos de contacto",
  * required=true,
  * in="body",
  * type="string",
    * @SWG\Schema(
    * type="object",
    * example = {
		"user":"1","nombre":"mi contacto","telefono":"312312312","email":"emergencia@gmail.com"
		}
    * )    
  * )
	* )
	*/
	public function newAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = json_decode($request->getContent(),true);
		$user = $em->getRepository('AppBundle:User')->find($data['user']);
		$contacto = new ContactoEmergencia;
		$contacto->setUser($user);
		$contacto->setNombre($data['nombre']);
		$contacto->setTelefono($data['telefono']);
		$contacto->setEmail($data['email']);
		$em->persist($contacto);
		$em->flush();
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

		/**
	* @Route("/{user}/all", methods="GET")
	* @SWG\Response(
	*   response=200,
	*   description="mis contactos de emergencia ",
	* )
	*/
	public function indexAction($user,Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$contactos = $em->getRepository('AppBundle:ContactoEmergencia')->findByUser($user); 
		$view = $this->view($contactos,200);
		return $this->handleView($view);
	}
}
