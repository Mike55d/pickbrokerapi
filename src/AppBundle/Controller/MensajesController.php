<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\Mensaje;

/**
* @Route("/api/mensajes")
* @SWG\Tag(name="mensajes")
*/
class MensajesController extends AbstractFOSRestController
{
	/**
	* @Route("/new", methods="POST")
	* @SWG\Response(
	*  response=200,
	*  description="enviar mensaje",
	* )
		* @SWG\Parameter(
  * name="data",
  * description="enviar mensaje",
  * required=true,
  * in="body",
  * type="string",
    * @SWG\Schema(
    * type="object",
    * example = {
		"envia":1,"recibe":"2","mensaje":"quiero que nos reunamos",
		}
    * )    
  * )
	* )
	*/
	public function newAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = json_decode($request->getContent(),true);
		$envia = $em->getRepository('AppBundle:User')->find($data['envia']);
		$recibe = $em->getRepository('AppBundle:User')->find($data['recibe']);
		$mensaje = new Mensaje;
		$mensaje->setEnvia($envia);
		$mensaje->setRecibe($recibe);
		$mensaje->setMensaje($data['mensaje']);
		$mensaje->setDate(new \Datetime());
		$em->persist($mensaje);
		$em->flush();
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/{user}/all", methods="GET")
	* @SWG\Response(
	*   response=200,
	*   description="mis mensajes",
	* )
	*/
	public function indexAction($user, Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$llamadas = $em->getRepository('AppBundle:Mensaje')->findMessages($user); 
		$view = $this->view($llamadas,200);
		return $this->handleView($view);
	}


}
