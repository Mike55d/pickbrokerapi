<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\Inmueble;
use AppBundle\Entity\ServiciosInmuebles;


/**
* @Route("/api/publicaciones")
* @SWG\Tag(name="publicaciones")
*/
class PublicacionesController extends AbstractFOSRestController
{
	/**
	* @Route("/" , methods="GET")
	* @SWG\Response(
	* response=200,
	* description="obtener todas las publicaciones",
	* )
	*/
	public function indexAction()
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = $em->getRepository('AppBundle:Inmueble')->findActive(); 
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/{user}/misPropiedades" , methods="GET")
	* @SWG\Response(
	* response=200,
	* description="obtener mis Propiedades",
	* )
	*/
	public function misPropiedadesAction($user)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = $em->getRepository('AppBundle:Inmueble')->findByUser($user); 
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/calcularPrecio" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="calcular precio de la propiedad",
	* )
	*/
	public function calcularPrecioAction()
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = 56479444;
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/new" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="guardar publicacion",
	* ),
	* @SWG\Parameter(
	* name="data",
	* description="calcular precio de la propiedad",
	* required=true,
	* in="body",
	* type="string",
  * @SWG\Schema(
  * type="object",
  * example = {
  "id_usuario":"","id_status_publicacion":3,"id_tipo_publicacion":"","id_tipo_inmueble":"",
	"titulo":"","nombre":"","inmobiliaria":"","precio":"","direccion":"","lat_long":"","descripcion":"",
	"terreno":"","construido":"","recamaras":"","banios":"","cochera":"","antiguedad":"","fecha_publicacion":"",
	"fecha_vencimiento":"","caracteristicas":null,"servicios":null,
	}
  *)    
	* )
	*/
	public function guardarAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = json_decode($request->getContent(), true);
		$inmueble = new Inmueble;
		$user = $em->getRepository('AppBundle:User')->find($data['id_usuario']); 
		$inmueble->setuser($user);
		$tipoPublicacion = $em->getRepository('AppBundle:TipoPublicacion')->find($data['id_tipo_publicacion']); 
		$inmueble->setTipoPublicacion($tipoPublicacion);
		$tipoInmueble = $em->getRepository('AppBundle:TipoInmueble')->find($data['id_tipo_inmueble']); 
		$inmueble->setTipoInmueble($tipoInmueble);
		$status = $em->getRepository('AppBundle:StatusPublicacion')->find(1); 
		$inmueble->setStatus($status);
		$inmueble->setTitulo($data['titulo']);
		$inmueble->setInmobiliaria($data['inmobiliaria']);
		$inmueble->setPrecio($data['precio']);
		$inmueble->setDireccion($data['direccion']);
		$inmueble->setLatLong($data['lat_long']);
		$inmueble->setDescripcion($data['descripcion']);
		$inmueble->setTerreno($data['terreno']);
		$inmueble->setConstruido($data['construido']);
		$inmueble->setRecamaras($data['recamaras']);
		$inmueble->setBanios($data['banios']);
		$inmueble->setCochera($data['cochera']);
		$inmueble->setServicios(implode(',',$data['servicios']));
		$inmueble->setCaracteristicas(implode(',',$data['caracteristicas']));
		$inmueble->setAntiguedad($data['antiguedad']);
		$inmueble->setFechaPublicacion(new \DateTime());
		$inmueble->setFechaVencimiento(new \DateTime());		
		$em->persist($inmueble);
		$em->flush();
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/statusChange" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="cambiar status inmueble",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="cambiar status de inmueble",
  * required=true,
  * in="body",
  * type="string",
  * @SWG\Schema(
  * type="object",
  * example = {
	"inmueble":"1"
	}
  * )    
  * )
	* )
	*/
	public function statusAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$data = json_decode($request->getContent(),true);
		$inmueble= $em->getRepository('AppBundle:Inmueble')
		->find($data['inmueble']); 
		$inactivo=$em->getRepository('AppBundle:StatusPublicacion')->find(2);
		$activo=$em->getRepository('AppBundle:StatusPublicacion')->find(1);
		if ($inmueble->getStatus()->getId() == 1 || $inmueble->getStatus()->getId() == 3) {
			$inmueble->setStatus($inactivo);
		}else{
			$inmueble->setStatus($activo);
		}
		$em->flush();
		$view = $this->view($inmueble,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/buscar" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="buscar inmueble por parametros",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="parametros de busqueda",
  * required=true,
  * in="body",
  * type="string",
  * @SWG\Schema(
  * type="object",
  * example = {"min":"100" ,"max":"500","tipo":"1"}
  * )    
  * )
	* )
	*/
	public function buscarAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$data = json_decode($request->getContent(),true);
		$inmuebles = $em->getRepository('AppBundle:Inmueble')->buscar($data); 
		$view = $this->view($inmuebles,200);
		return $this->handleView($view);
	}

}
