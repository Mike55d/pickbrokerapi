<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\Cita;

/**
* @Route("/api/preguntasFrecuentes")
* @SWG\Tag(name="preguntasFrecuentes")
*/
class PreguntasFrecuentesController extends AbstractFOSRestController
{
		/**
	* @Route("/" , methods="GET")
	* @SWG\Response(
	* response=200,
	* description="obtener preguntas frecuentes",
	* )
	*/
	public function indexAction()
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = $em->getRepository('AppBundle:PreguntasFrecuentes')->findAll(); 
		$view = $this->view($data,200);
		return $this->handleView($view);
	}
}
