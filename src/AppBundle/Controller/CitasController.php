<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\Cita;

/**
* @Route("/api/citas")
* @SWG\Tag(name="citas")
*/
class CitasController extends AbstractFOSRestController
{
	/**
	* @Route("/new", methods="POST")
	* @SWG\Response(
	*   response=200,
	*   description="agendar cita",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="datos para agendar una cita",
  * required=true,
  * in="body",
  * type="string",
    * @SWG\Schema(
    * type="object",
    * example = {
		"user":"1","socio":"2","fecha":"5/11/2019","hora":"15:06:00","inmueble":"2","comentario":"deberiamos reunirnos"
		}
    * )    
  * )
	* )
	*/
	public function newAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = json_decode($request->getContent(),true);
		$user = $em->getRepository('AppBundle:User')->find($data['user']);
		$socio = $em->getRepository('AppBundle:User')->find($data['socio']);
		$inmueble = $em->getRepository('AppBundle:Inmueble')->find($data['inmueble']); 
		$cita = new Cita ;
		$cita->setUser($user);
		$cita->setSocio($socio);
		$cita->setInmueble($inmueble);
		$cita->setComentario($data['comentario']);
		$cita->setFecha(new \Datetime($data['fecha']));
		$cita->setHora(new \Datetime($data['hora']));
		$em->persist($cita);
		$em->flush();
		$view = $this->view($data,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/{user}/all", methods="GET")
	* @SWG\Response(
	*   response=200,
	*   description="mis Citas",
	* )
	*/
	public function indexAction($user, Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$llamadas = $em->getRepository('AppBundle:Cita')->findByUser($user); 
		$view = $this->view($llamadas,200);
		return $this->handleView($view);
	}

}
