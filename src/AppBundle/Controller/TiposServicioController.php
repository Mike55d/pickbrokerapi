<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;


/**
* @Route("/api/tipos_servicio")
* @SWG\Tag(name="tipos de servicios")
*/
class TiposServicioController extends AbstractFOSRestController
{
    /**
		* @Route("/" , methods="GET")
		* @SWG\Response(
		* response=200,
		* description="obtener todos los servicios",
		* )
		*/
		public function indexAction()
		{
			$em =$this->getDoctrine()->getManager(); 
			$data = $em->getRepository('AppBundle:TipoServicio')->findAll(); 
			$view = $this->view($data,200);
			return $this->handleView($view);
		}

}
