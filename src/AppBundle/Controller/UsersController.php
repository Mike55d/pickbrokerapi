<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\User;


/**
* @Route("/api/users")
* @SWG\Tag(name="users")
*/
class UsersController extends Controller
{
	/**
	* @Route("/register" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="registrar nuevo usuario",
	* )    
	* @SWG\Parameter(
  * name="data",
  * description="registrar usuario",
  * required=true,
  * in="body",
  * type="string",
    * @SWG\Schema(
    * type="object",
    * example = {
		"id_nivel":5,"nombre":"","apellido":"","login":"",
		"pass_1":"","telf_contacto":"",
		}
    * )    
  * )
	* )
	*/
	public function registerAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$data = json_decode($request->getContent(), true); 
		$user = new User;
		$role = $em->getRepository('AppBundle:Roles')->find($data['id_nivel']); 
		$user->setRol($role);
		$user->setUsername($data['login']);
		$user->setName($data['nombre']);
		$user->setLastName($data['apellido']);
		$password = $this->get('security.password_encoder')
		->encodePassword($user,$data['pass_1']);
		$user->setPassword($password);
		$user->setPhone($data['telf_contacto']);
		$user->setActive(1);
		$user->setRoles(["ROLE_USER"]);
		$user->setRola('USER');
		$em->persist($user);
		$em->flush();
		//$view = $this->view($data,200);
		return $data;
	}
}
