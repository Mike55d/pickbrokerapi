<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

/**
* @Route("/api/servicios")
* @SWG\Tag(name="Servicios")
*/
class ServiciosController extends AbstractFOSRestController
{
	/**
	* @Route("/", methods="GET")
	* @SWG\Response(
	*   response=200,
	*   description="todos los servicios",
	* )
	*/
	public function indexAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$categorias = $em->getRepository('AppBundle:Servicio')->findAll(); 
		$view = $this->view($categorias,200);
		return $this->handleView($view);
	}
}
