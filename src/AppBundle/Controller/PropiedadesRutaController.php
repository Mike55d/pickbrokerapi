<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\PropiedadesRuta;


/**
* @Route("/api/propiedadesRuta")
* @SWG\Tag(name="propiedadesRuta")
*/
class PropiedadesRutaController extends AbstractFOSRestController
{
	/**
	* @Route("/" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="obtener todas las propiedades en ruta",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="cambiar status de inmueble",
  * required=true,
  * in="body",
  * type="string",
  * @SWG\Schema(
  * type="object",
  * example = {"user":"1"}
  * )    
  * )
	* )
	*/
	public function indexAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data  = json_decode($request->getContent(),true);
		$propiedades = $em->getRepository('AppBundle:PropiedadesRuta')->findByUser($data['user']); 
		$view = $this->view($propiedades,200);
		return $this->handleView($view);
	}

	/**
	* @Route("/new" , methods="POST")
	* @SWG\Response(
	* response=200,
	* description="guardar propiedad en ruta",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="datos de propiedad",
  * required=true,
  * in="body",
  * type="string",
  * @SWG\Schema(
  * type="object",
  * example = {
	"user":"1","direccion":"caracas venezuela","latLong":"31.69036380000001,-106.42454780000003",
	"comentarios":"muy bonita propiedad","fecha":"10-08-2019","imagen":"ninguna"}
  * )    
  * )
	* )
	*/
	public function newAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data  = json_decode($request->getContent(),true);
		$propiedadRuta = new PropiedadesRuta;
		$user= $em->getRepository('AppBundle:User')->find($data['user']);
		$propiedadRuta->setUser($user);
		$propiedadRuta->setDireccion($data['direccion']);
		$propiedadRuta->setLatLong($data['latLong']);
		$propiedadRuta->setComentarios($data['comentarios']);
		$propiedadRuta->setFecha(new \Datetime($data['fecha']));
		$propiedadRuta->setImagen('none');
		$em->persist($propiedadRuta);
		$em->flush();
		$view = $this->view($propiedadRuta,200);
		return $this->handleView($view);
	}
}
