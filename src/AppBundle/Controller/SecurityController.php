<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\AbstractFOSRestController;



class SecurityController extends Controller
{
    /**
     * @Route("api/login" , name="login" , methods="POST"),
    * @SWG\Response(
    *     response=200,
    *     description="login endpoint",
    * ),
    * @SWG\Parameter(
    * name="data",
    * description="logear usuario",
    * required=true,
    * in="body",
    * type="string",
        * @SWG\Schema(
        * type="object",
        * example = {"username":"mike","password":"1234"},
        * )    
    * )
    * @SWG\Tag(name="login")
    */
    public function loginAction(Request $request)
    {
        $em =$this->getDoctrine()->getManager(); 
    	$authenticationUtils = $this->get('security.authentication_utils');
		// get the login error if there is one
    	$error = $authenticationUtils->getLastAuthenticationError();
		// last username entered by the user
    	$lastUsername = $authenticationUtils->getLastUsername();
        /*
    	return $this->render(
    		'AppBundle:Security:login2.html.twig',
    		array(
			// last username entered by the user
    			'last_username' => $lastUsername,
    			'error' => $error,
    		)
    	);
        */
        $parametersAsArray = json_decode($request->getContent(), true);
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($parametersAsArray['username']);  
        return $user;
    }

}
