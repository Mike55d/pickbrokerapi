<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;


/**
* @Route("/api/test")
* @SWG\Tag(name="rewards")
*/
class TestController extends AbstractFOSRestController
{
	/**
	* @Route("/" , name="test" , methods="GET")
	* @SWG\Response(
	*     response=200,
	*     description="return test",
	* )
	*/
	public function indexAction()
	{
		$data = ['nombre'=>'mike'];
		$view = $this->view($data,200);

		return $this->handleView($view);
	}

}
