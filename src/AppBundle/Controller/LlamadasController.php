<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use AppBundle\Entity\Llamada;

/**
* @Route("/api/llamadas")
* @SWG\Tag(name="llamadas")
*/
class LlamadasController extends AbstractFOSRestController
{
	/**
	* @Route("/new", methods="POST")
	* @SWG\Response(
	*   response=200,
	*   description="guardar llamada",
	* )
	* @SWG\Parameter(
  * name="data",
  * description="guardar llamada en historial",
  * required=true,
  * in="body",
  * type="string",
    * @SWG\Schema(
    * type="object",
    * example = {
		"user":"1","called":"2"
		}
    * )    
  * )
	* )
	*/
	public function newAction(Request $request)
	{
		$em =$this->getDoctrine()->getManager(); 
		$data = json_decode($request->getContent(),true);
		$user = $em->getRepository('AppBundle:User')->find($data['user']);
		$called = $em->getRepository('AppBundle:User')->find($data['called']);
		$llamada = new Llamada ;
		$llamada->setUser($user);
		$llamada->setCalled($called);
		$llamada->setFecha(new \Datetime());
		$em->persist($llamada);
		$em->flush();
		$view = $this->view($data,200);
		return $this->handleView($view);
	}


	/**
	* @Route("/{user}/all", methods="GET")
	* @SWG\Response(
	*   response=200,
	*   description="mis llamadas",
	* )
	*/
	public function indexAction($user, Request $request)
	{
		$em =$this->getDoctrine()->getManager();
		$llamadas = $em->getRepository('AppBundle:Llamada')->findByUser($user); 
		$view = $this->view($llamadas,200);
		return $this->handleView($view);
	}

}
